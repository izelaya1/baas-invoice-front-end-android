package com.example.baas_invoice_android.data.local.dto;

import androidx.lifecycle.ViewModel;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequestDto extends ViewModel {

    private InvoiceHeaderDto header;

    private List<InvoiceDetailDto> details;

    public InvoiceHeaderDto getHeader() {
        return header;
    }

    public void setHeader(InvoiceHeaderDto header) {
        this.header = header;
    }

    public List<InvoiceDetailDto> getDetails() {
        return details;
    }

    public void setDetails(List<InvoiceDetailDto> details) {
        this.details = details;
    }
}
