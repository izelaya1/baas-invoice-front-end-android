package com.example.baas_invoice_android.data.local.dto;

import androidx.lifecycle.ViewModel;

import java.math.BigDecimal;
import java.util.zip.ZipError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceDetailDto extends ViewModel {

    private Integer offerQuantity;

    private String offerDescription;

    private BigDecimal offerUnitValue;

    private BigDecimal itemSubtotal;

    private BigDecimal itemUnitTax1;

    private BigDecimal itemUnitTax2;

    private BigDecimal itemUnitTax3;

    public Integer getOfferQuantity() {
        return offerQuantity;
    }

    public void setOfferQuantity(Integer offerQuantity) {
        this.offerQuantity = offerQuantity;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public BigDecimal getOfferUnitValue() {
        return offerUnitValue;
    }

    public void setOfferUnitValue(BigDecimal offerUnitValue) {
        this.offerUnitValue = offerUnitValue;
    }

    public BigDecimal getItemSubtotal() {
        return itemSubtotal;
    }

    public void setItemSubtotal(BigDecimal itemSubtotal) {
        this.itemSubtotal = itemSubtotal;
    }

    public BigDecimal getItemUnitTax1() {
        return itemUnitTax1;
    }

    public void setItemUnitTax1(BigDecimal itemUnitTax1) {
        this.itemUnitTax1 = itemUnitTax1;
    }

    public BigDecimal getItemUnitTax2() {
        return itemUnitTax2;
    }

    public void setItemUnitTax2(BigDecimal itemUnitTax2) {
        this.itemUnitTax2 = itemUnitTax2;
    }

    public BigDecimal getItemUnitTax3() {
        return itemUnitTax3;
    }

    public void setItemUnitTax3(BigDecimal itemUnitTax3) {
        this.itemUnitTax3 = itemUnitTax3;
    }

    public static InvoiceDetailDto empty() {
        InvoiceDetailDto dto = new InvoiceDetailDto();
        dto.setOfferQuantity(1);
        dto.setOfferUnitValue(BigDecimal.ZERO);
        dto.setOfferUnitValue(BigDecimal.ZERO);
        dto.setItemUnitTax1(BigDecimal.ZERO);
        dto.setItemUnitTax2(BigDecimal.ZERO);
        dto.setItemUnitTax3(BigDecimal.ZERO);
        return dto;
    }

}
