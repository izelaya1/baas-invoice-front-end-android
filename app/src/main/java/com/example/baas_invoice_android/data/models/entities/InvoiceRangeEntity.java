package com.example.baas_invoice_android.data.models.entities;

import com.example.baas_invoice_android.utils.enums.EntityStatus;

import java.time.LocalDate;

public class InvoiceRangeEntity {

    private String rangeId;

    private String approvedRangePrefix;

    private String printAuthorizationKey;

    private LocalDate rangeDueDate;

    private Integer rangeStart;

    private Integer rangeCurrent;

    private Integer rangeEnd;

    private EntityStatus rangeStatus;

    private LocationEntity location;
}
