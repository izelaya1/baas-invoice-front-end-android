package com.example.baas_invoice_android.data.local.dto;

import android.location.Location;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.ViewModel;

import com.example.baas_invoice_android.BR;
import com.example.baas_invoice_android.data.models.entities.LocationEntity;
import com.example.baas_invoice_android.utils.enums.InvoiceType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceHeaderDto extends BaseObservable {

    private String tenantId;

    private String accountNumber;

    private String accountName;

    private String accountTaxId;

    private InvoiceType invoiceType;

    private String accountExonerationId;

    private LocalDateTime invoiceDate;

    private String invoiceLocationId;

    private Location invoiceLocation;

    private String invoicePeriod;

    private LocalDate invoiceDueDate;

    private BigDecimal invoiceDiscount;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public LocalDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountTaxId() {
        return accountTaxId;
    }

    public void setAccountTaxId(String accountTaxId) {
        this.accountTaxId = accountTaxId;
    }

    @Bindable
    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
        notifyPropertyChanged(BR.invoiceType);
    }

    public Location getInvoiceLocation() {
        return invoiceLocation;
    }

    public void setInvoiceLocation(Location invoiceLocation) {
        this.invoiceLocation = invoiceLocation;
    }

    public String getAccountExonerationId() {
        return accountExonerationId;
    }

    public void setAccountExonerationId(String accountExonerationId) {
        this.accountExonerationId = accountExonerationId;
    }

    public String getInvoiceLocationId() {
        return invoiceLocationId;
    }

    public void setInvoiceLocationId(String invoiceLocationId) {
        this.invoiceLocationId = invoiceLocationId;
    }

    public String getInvoicePeriod() {
        return invoicePeriod;
    }

    public void setInvoicePeriod(String invoicePeriod) {
        this.invoicePeriod = invoicePeriod;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public BigDecimal getInvoiceDiscount() {
        return invoiceDiscount;
    }

    public void setInvoiceDiscount(BigDecimal invoiceDiscount) {
        this.invoiceDiscount = invoiceDiscount;
    }

    public static InvoiceHeaderDto empty() {
        return new InvoiceHeaderDto();
    }

}
