package com.example.baas_invoice_android.data.models.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.baas_invoice_android.utils.enums.EntityStatus;

@Entity
public class InvoiceTemplateEntity {

    @PrimaryKey
    private String templateId;

    @ColumnInfo
    private LocationEntity location;

    @ColumnInfo
    private String templateBody;

    @ColumnInfo
    private EntityStatus templateStatus;
}
