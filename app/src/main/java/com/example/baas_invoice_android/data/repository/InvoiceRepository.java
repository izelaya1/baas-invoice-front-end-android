package com.example.baas_invoice_android.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.baas_invoice_android.data.local.dto.InvoicePdfDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceRequestDto;
import com.example.baas_invoice_android.service.ApiClient;
import com.example.baas_invoice_android.data.models.entities.InvoiceEntity;
import com.example.baas_invoice_android.service.InvoiceService;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceRepository {

    private InvoiceService invoiceService;

    public InvoiceRepository() {
        invoiceService = ApiClient.getRetrofitInstance().create(InvoiceService.class);
    }

    public Single<InvoiceEntity> generateInvoice(InvoiceRequestDto invoiceEntity){
        return invoiceService.generateInvoice(invoiceEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<InvoicePdfDto> getInvoiceBlob(String blobName) {
        return invoiceService.retrieveBlob(blobName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<List<InvoiceEntity>> getInvoices(){

        MutableLiveData<List<InvoiceEntity>> invoiceLiveData = new MutableLiveData<>();

        invoiceService.getInvoices()
                .enqueue(new Callback<List<InvoiceEntity>>() {
                    @Override
                    public void onResponse(Call<List<InvoiceEntity>> call, Response<List<InvoiceEntity>> response) {
                        if(response.isSuccessful()){
                            invoiceLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<InvoiceEntity>> call, Throwable t) {
                        // Handle failure
                    }
                });
        return invoiceLiveData;
    }

    //Pdf Generator.
}
