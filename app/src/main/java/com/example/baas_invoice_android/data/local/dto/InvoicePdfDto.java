package com.example.baas_invoice_android.data.local.dto;

public class InvoicePdfDto {

    private String blobName;

    private byte[] pdfData;

    public InvoicePdfDto(String blobName, byte[] pdfData) {
        this.blobName = blobName;
        this.pdfData = pdfData;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public byte[] getPdfData() {
        return pdfData;
    }

    public void setPdfData(byte[] pdfData) {
        this.pdfData = pdfData;
    }
}
