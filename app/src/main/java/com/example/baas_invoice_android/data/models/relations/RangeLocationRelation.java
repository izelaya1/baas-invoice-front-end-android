package com.example.baas_invoice_android.data.models.relations;


import android.location.Location;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.baas_invoice_android.data.models.entities.InvoiceRangeEntity;

import java.util.List;

public class RangeLocationRelation {

    @Embedded
    public Location location;

    @Relation(parentColumn = "location_id", entityColumn = "range_id")
    public List<InvoiceRangeEntity> invoiceRanges;
}
