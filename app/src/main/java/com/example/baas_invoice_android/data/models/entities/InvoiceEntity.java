package com.example.baas_invoice_android.data.models.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
public class InvoiceEntity {

    @PrimaryKey
    @ColumnInfo(name = "invoice_id")
    private String invoiceId;

    @ColumnInfo
    private String invoiceUrl;

    @ColumnInfo
    private LocalDate invoiceDate;

    @ColumnInfo
    private LocalDate voidDueDate;

    @ColumnInfo
    private LocalDate invoiceDueDate;

    @ColumnInfo
    private String invoiceNumber;

    @ColumnInfo
    private BigDecimal invoiceSubtotal;

    @ColumnInfo
    private BigDecimal invoiceTax1;

    @ColumnInfo
    private BigDecimal invoiceTax2;

    @ColumnInfo
    private BigDecimal invoiceDiscount;

    @ColumnInfo
    private LocationEntity invoiceLocation;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceUrl() {
        return invoiceUrl;
    }

    public void setInvoiceUrl(String invoiceUrl) {
        this.invoiceUrl = invoiceUrl;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public LocalDate getVoidDueDate() {
        return voidDueDate;
    }

    public void setVoidDueDate(LocalDate voidDueDate) {
        this.voidDueDate = voidDueDate;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getInvoiceSubtotal() {
        return invoiceSubtotal;
    }

    public void setInvoiceSubtotal(BigDecimal invoiceSubtotal) {
        this.invoiceSubtotal = invoiceSubtotal;
    }

    public BigDecimal getInvoiceTax1() {
        return invoiceTax1;
    }

    public void setInvoiceTax1(BigDecimal invoiceTax1) {
        this.invoiceTax1 = invoiceTax1;
    }

    public BigDecimal getInvoiceTax2() {
        return invoiceTax2;
    }

    public void setInvoiceTax2(BigDecimal invoiceTax2) {
        this.invoiceTax2 = invoiceTax2;
    }

    public BigDecimal getInvoiceDiscount() {
        return invoiceDiscount;
    }

    public void setInvoiceDiscount(BigDecimal invoiceDiscount) {
        this.invoiceDiscount = invoiceDiscount;
    }
}
