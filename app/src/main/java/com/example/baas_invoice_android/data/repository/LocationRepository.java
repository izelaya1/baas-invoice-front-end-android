package com.example.baas_invoice_android.data.repository;

import com.example.baas_invoice_android.data.models.entities.LocationEntity;
import com.example.baas_invoice_android.service.ApiClient;
import com.example.baas_invoice_android.service.LocationService;
import com.example.baas_invoice_android.utils.enums.EntityStatus;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LocationRepository {

    private LocationService locationService;
    public LocationRepository() {
        this.locationService = ApiClient.getRetrofitInstance().create(LocationService.class);
    }

    public Single<LocationEntity> findById(final String locationId) {
        //return locationService.findById(locationId)
        //       .subscribeOn(Schedulers.io())
        //     .observeOn(AndroidSchedulers.mainThread());
        LocationEntity mockLocation = new LocationEntity();
        mockLocation.setLocationId("123");
        mockLocation.setLocationName("BaaS Technologies. Centro Comercial C.A.");
        mockLocation.setLocationStatus(EntityStatus.ACTIVE);
        mockLocation.setTenantId("Tenant 2");
        return Single.just(mockLocation);
    }

}
