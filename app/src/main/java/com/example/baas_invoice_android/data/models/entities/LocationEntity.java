package com.example.baas_invoice_android.data.models.entities;

import com.example.baas_invoice_android.utils.enums.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
public class LocationEntity {

    private String locationId;

    private String tenantId;

    private String locationName;

    private EntityStatus locationStatus;

    public LocationEntity() { }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public EntityStatus getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(EntityStatus locationStatus) {
        this.locationStatus = locationStatus;
    }
}
