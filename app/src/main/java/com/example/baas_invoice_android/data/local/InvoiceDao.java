package com.example.baas_invoice_android.data.local;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.baas_invoice_android.data.models.entities.InvoiceEntity;

import java.util.List;

@Dao
public interface InvoiceDao {

    @Insert
    void insert(InvoiceEntity invoice);
    List<InvoiceEntity> getInvoices();

}
