package com.example.baas_invoice_android.utils.enums;

public enum EntityStatus {

    INACTIVE(0),
    ACTIVE(1);

    EntityStatus(Integer statusCode) {
        this.statusCode = statusCode;
    }

    private final Integer statusCode;
}
