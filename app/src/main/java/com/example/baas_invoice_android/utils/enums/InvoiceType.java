package com.example.baas_invoice_android.utils.enums;

public enum InvoiceType {

    CASH(1),
    CREDIT(2);

    private int value;

    InvoiceType(int i){
        value = i;
    }

    public int getValue(){
        return value;
    }
    public static InvoiceType fromValue(int value){
        for(InvoiceType e : InvoiceType.values()){
            if(e.getValue() == value){
                return e;
            }
        }
        throw new IllegalArgumentException("Invalid Invoice Type");
    }
}
