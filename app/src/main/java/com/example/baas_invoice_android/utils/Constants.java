package com.example.baas_invoice_android.utils;

public enum Constants {

    INVOICE_BASE_URL("http://localhost:8002/api/v1/invoices/");

    private final String value;

    Constants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
