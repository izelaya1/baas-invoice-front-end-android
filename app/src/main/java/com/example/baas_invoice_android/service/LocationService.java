package com.example.baas_invoice_android.service;

import com.example.baas_invoice_android.data.models.entities.LocationEntity;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface LocationService extends ApiService {

    @GET("locations/{locationId}/")
    Single<LocationEntity> findById(@Path("locationId") String locationId);


}
