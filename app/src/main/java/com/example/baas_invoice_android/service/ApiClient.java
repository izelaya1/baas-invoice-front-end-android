package com.example.baas_invoice_android.service;

import com.example.baas_invoice_android.utils.Constants;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(Constants.INVOICE_BASE_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
