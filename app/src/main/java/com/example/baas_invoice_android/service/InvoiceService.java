package com.example.baas_invoice_android.service;

import com.example.baas_invoice_android.data.local.dto.InvoicePdfDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceRequestDto;
import com.example.baas_invoice_android.data.models.entities.InvoiceEntity;
import com.example.baas_invoice_android.utils.Constants;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface InvoiceService extends ApiService {

    @POST
    Single<InvoiceEntity> generateInvoice(@Body InvoiceRequestDto entity);

    @GET("{blobName}/")
    Single<InvoicePdfDto> retrieveBlob(@Path("blobName") String blobName);

    @GET
    Call<List<InvoiceEntity>> getInvoices();

}
