package com.example.baas_invoice_android.service;

import com.example.baas_invoice_android.data.models.entities.InvoiceTemplateEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface InvoiceTemplateService {

    @POST("/templates")
    public Call<InvoiceTemplateEntity> postTemplate(@Body InvoiceTemplateEntity entity);

    @GET("/templates/{templateId}")
    public Call<InvoiceTemplateEntity> findTemplateById(@Path("templateId")
                                                            final String templateId);

}
