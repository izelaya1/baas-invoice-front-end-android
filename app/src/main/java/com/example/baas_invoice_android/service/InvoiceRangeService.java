package com.example.baas_invoice_android.service;

import com.example.baas_invoice_android.data.models.entities.InvoiceRangeEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface InvoiceRangeService extends ApiService {

    @GET("/ranges/{rangeId}")
    Call<InvoiceRangeEntity> findRangeById(@Path("rangeId") final String rangeId);

    @POST("/ranges")
    Call<InvoiceRangeEntity> postRange(@Body final InvoiceRangeEntity entity);

}
