package com.example.baas_invoice_android.ui.fragments;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baas_invoice_android.R;
import com.example.baas_invoice_android.data.local.dto.InvoiceDetailDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceHeaderDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceRequestDto;
import com.example.baas_invoice_android.data.models.entities.LocationEntity;
import com.example.baas_invoice_android.data.repository.LocationRepository;
import com.example.baas_invoice_android.databinding.InvoiceFragmentBinding;
import com.example.baas_invoice_android.ui.activities.MainActivity;
import com.example.baas_invoice_android.ui.adapters.InvoiceItemAdapter;
import com.example.baas_invoice_android.ui.dialogs.InvoicePreviewDialog;
import com.example.baas_invoice_android.ui.viewmodels.DatePickerViewModel;
import com.example.baas_invoice_android.ui.viewmodels.InvoiceTypeViewModel;
import com.example.baas_invoice_android.ui.viewmodels.repository.InvoiceRepoViewModel;
import com.google.android.material.tabs.TabLayout;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;


public class InvoiceFragment extends Fragment {

    public static final String TAG = "InvoiceFragment";
    private InvoiceFragmentBinding binding;
    private InvoiceRepoViewModel invoiceViewModel;
    private DatePickerViewModel datePickerViewModel;

    private InvoiceTypeViewModel invoiceTypeViewModel;
    private InvoiceHeaderDto invoiceHeaderDto;
    private List<InvoiceDetailDto> invoiceDetailDtoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private InvoiceItemAdapter invoiceItemAdapter;
    private LocationRepository locationRepository;
    private Disposable locationDisposable;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = InvoiceFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        invoiceViewModel = new ViewModelProvider(this).get(InvoiceRepoViewModel.class);
        datePickerViewModel = new ViewModelProvider(this).get(DatePickerViewModel.class);
        invoiceTypeViewModel = new ViewModelProvider(this).get(InvoiceTypeViewModel.class);

        datePickerViewModel.setContext(new WeakReference<>(requireContext()));

        binding.setInvoiceHeaderDto(invoiceHeaderDto);
        binding.setDatePickerViewModel(datePickerViewModel);
        binding.setInvoiceTypeViewModel(invoiceTypeViewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        invoiceHeaderDto = InvoiceHeaderDto.empty();
        recyclerView = view.findViewById(R.id.item_recycler_view);
        invoiceDetailDtoList.add(InvoiceDetailDto.empty());
        invoiceItemAdapter = new InvoiceItemAdapter(this.invoiceDetailDtoList);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setAdapter(invoiceItemAdapter);

        //Replace with Dependency Injection
        locationRepository = new LocationRepository();
        setViewListeners();
    }

    private void setViewListeners() {
        binding.locationQueryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findLocationById();
            }
        });
        binding.invoiceDetailAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invoiceItemAdapter.addItem(InvoiceDetailDto.empty());
            }
        });
        binding.invoiceDetailRemoveItem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                invoiceItemAdapter.removeItem();
            }
        });

        binding.invoiceDiscountSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected
                            (AdapterView<?> adapterView, View view, int position, long id) {
                        String selectedValue = adapterView.getItemAtPosition(position).toString();
                        try {
                            Number discountValue = new DecimalFormat("0.0#%").parse(selectedValue);
                            Log.d(TAG, "Discount value selected: " + discountValue);
                            invoiceHeaderDto.setInvoiceDiscount(new BigDecimal(discountValue.toString()));
                        }
                        catch(ParseException e) {
                            Log.e(TAG, "Error parsing value: " + e.getMessage());
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        invoiceHeaderDto.setInvoiceDiscount(BigDecimal.ZERO);
                    }
                }
        );
        binding.invoiceSummaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSummarizedInvoicePreview();
            }
        });

    }

    private void showSummarizedInvoicePreview() {
        InvoicePreviewDialog invoicePreviewDialog =
                new InvoicePreviewDialog(this.invoiceHeaderDto, this.invoiceDetailDtoList);
        bindTextProperties();
        invoicePreviewDialog.show(getChildFragmentManager(), InvoicePreviewDialog.TAG);
    }


    private void bindTextProperties() {
        invoiceHeaderDto.setAccountName(this.binding.formAccName.getText().toString());
        invoiceHeaderDto.setAccountTaxId(this.binding.formTaxId.getText().toString());

        final LocalDateTime invoiceDate = this.datePickerViewModel.getSelectedDate().getValue()
                != null ? this.datePickerViewModel.getSelectedDate().getValue().atStartOfDay()
                : LocalDate.now().atStartOfDay();

        invoiceHeaderDto.setInvoiceDate(invoiceDate);
        invoiceHeaderDto.setInvoiceType(this.invoiceTypeViewModel.selectedInvoiceType.get());
        invoiceHeaderDto.setInvoiceLocationId("");
    }

    //TODO: CONSTRUCT INVOICE REQUEST DTO.
    private void constructRequestDto(InvoiceRequestDto invoiceRequestDto) {
        invoiceRequestDto.setHeader(this.invoiceHeaderDto);
        invoiceRequestDto.setDetails(invoiceDetailDtoList);
    }
    private void findLocationById() {
        locationDisposable = locationRepository.findById(binding.formLocationId.toString())
                .subscribe(entity -> {
                        displayLocationInTextView(entity);
                    },
                    throwable -> {
                        Log.e(TAG, "Error fetching location." + throwable.getMessage());
                        binding.textLocationJson
                                .setText("Error fetching Location." + throwable.getMessage());
                });
    }

    //TODO: GENERATE INVOICE WITH VIEW MODEL DATA.
    private void generateInvoice() {
        InvoiceRequestDto requestDto = new InvoiceRequestDto();
        constructRequestDto(requestDto);
    }


    private void displayLocationInTextView(LocationEntity location) {

        final StringBuilder textView = new StringBuilder
                ("<b>Tenant ID: </b>")
                .append("<i>").append(location.getTenantId()).append("</i>").append("<br>")
                .append("<b>Location Name: </b>")
                .append("<i>").append(location.getLocationName()).append("</i>").append("<br>")
                .append("<b>Location Status: </b>")
                .append("<i>").append(location.getLocationStatus()).append("</i>");

        binding.textLocationJson.setText(Html
                .fromHtml(textView.toString(), HtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        if(locationDisposable != null && !locationDisposable.isDisposed()){
            locationDisposable.dispose();
        }
    }
}
