package com.example.baas_invoice_android.ui.dialogs;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import com.example.baas_invoice_android.ui.viewmodels.DatePickerViewModel;

import java.time.LocalDate;
import java.util.Calendar;

public class DatePickerDialogHandler{

    public static void showDatePickerDialog(Context context, DatePickerViewModel datePickerViewModel) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        LocalDate selectedDate = LocalDate.of(i, i1, i2);
                        datePickerViewModel.onSelectedDate(selectedDate);
                    }
                },  year, month, dayOfMonth);
        datePickerDialog.show();
    }
}
