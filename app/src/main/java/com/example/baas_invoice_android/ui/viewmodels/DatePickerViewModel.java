package com.example.baas_invoice_android.ui.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.baas_invoice_android.ui.dialogs.DatePickerDialogHandler;

import java.lang.ref.WeakReference;
import java.time.LocalDate;

public class DatePickerViewModel extends ViewModel {

    private WeakReference<Context> context;
    private MutableLiveData<LocalDate> selectedDate = new MutableLiveData<>();

    public DatePickerViewModel() {}
    public LiveData<LocalDate> getSelectedDate() {
        return selectedDate;
    }
    public void setSelectedDate(LocalDate date){
        selectedDate.setValue(date);
    }
    public void requestDatePickerDialog() {
        DatePickerDialogHandler.showDatePickerDialog(this.context.get(), this);
    }
    public void onSelectedDate(LocalDate date) {
        selectedDate.setValue(date);
    }

    public void setContext(WeakReference<Context> context) {
        this.context = context;
    }

}