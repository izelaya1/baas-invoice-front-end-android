package com.example.baas_invoice_android.ui.viewmodels;

import android.view.View;
import android.widget.AdapterView;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import java.util.Arrays;
import java.util.List;

public class SpinnerViewModel extends ViewModel {

    private final MutableLiveData<Integer> selectedInteger = new MutableLiveData<>();
    private final MutableLiveData<List<Integer>> integerList = new MutableLiveData<>();

    public SpinnerViewModel() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        integerList.setValue(integers);
        selectedInteger.setValue(integers.get(0));
    }

    public MutableLiveData<Integer> getSelectedInteger() {
        return selectedInteger;
    }

    public MutableLiveData<List<Integer>> getIntegerList() {
        return integerList;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.selectedInteger.setValue((Integer) parent.getItemAtPosition(position));
    }
}
