package com.example.baas_invoice_android.ui.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.baas_invoice_android.ui.fragments.InvoiceFragment;

public class InvoiceFragmentAdapter extends FragmentStateAdapter {
    private static final int NUM_PAGES = 1;

    public InvoiceFragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = null;

        switch(position){
            case 0:
                fragment = new InvoiceFragment();

            default:
                fragment = new InvoiceFragment();
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return NUM_PAGES;
    }
}
