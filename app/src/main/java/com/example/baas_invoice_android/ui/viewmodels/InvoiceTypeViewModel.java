package com.example.baas_invoice_android.ui.viewmodels;

import android.widget.RadioGroup;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

import com.example.baas_invoice_android.R;
import com.example.baas_invoice_android.utils.enums.InvoiceType;

public class InvoiceTypeViewModel extends ViewModel {

    public final ObservableField<InvoiceType> selectedInvoiceType =
            new ObservableField<>(InvoiceType.CASH);

    public void onRadioButtonClicked(InvoiceType invoiceType) {
        selectedInvoiceType.set(invoiceType);
    }

    @BindingAdapter("invoiceType")
    public static void setInvoiceType(RadioGroup radioGroup, ObservableField<InvoiceType> invoiceType) {
        int checkedId = radioGroup.getCheckedRadioButtonId();
        if (checkedId == R.id.radio_button_cash) {
            invoiceType.set(InvoiceType.CASH);
        } else if (checkedId == R.id.radio_button_credit) {
            invoiceType.set(InvoiceType.CREDIT);
        }
    }
}
