package com.example.baas_invoice_android.ui.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baas_invoice_android.data.local.dto.InvoiceDetailDto;
import com.example.baas_invoice_android.databinding.InvoiceItemRowBinding;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Stack;

public class InvoiceItemAdapter extends
        RecyclerView.Adapter<InvoiceItemAdapter.ItemViewHolder> {
    private static final String TAG = "InvoiceItemAdapter";
    private Stack<Integer> lastAddedPositions = new Stack<>();
    private List<InvoiceDetailDto> invoiceItemList;
    public InvoiceItemAdapter(List<InvoiceDetailDto> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private InvoiceItemRowBinding binding;
        public ItemViewHolder(InvoiceItemRowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bind(InvoiceDetailDto invoiceDetailDto) {
            binding.setInvoiceDetailDto(invoiceDetailDto);
            binding.executePendingBindings();
        }
        public InvoiceItemRowBinding getBinding() {
            return this.binding;
        }
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        InvoiceItemRowBinding binding = InvoiceItemRowBinding
                .inflate(layoutInflater, parent, false);

        return new ItemViewHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        InvoiceDetailDto invoiceDetailDto = invoiceItemList.get(position);
        bindSpinners(holder, invoiceDetailDto);
        bindTextProperties(holder, invoiceDetailDto);
        holder.bind(invoiceDetailDto);

    }
    @Override
    public int getItemCount() {
        return invoiceItemList.size();
    }

    private void bindTextProperties(ItemViewHolder holder, InvoiceDetailDto invoiceDetailDto) {

        final InvoiceItemRowBinding localBinding = holder.getBinding();

        final String insertedValue = localBinding.formItemValue.getText().toString();
        if (!insertedValue.isEmpty()) {
            Log.d(TAG, "Inserted Offer Unit Value: " + insertedValue);
            final BigDecimal offerValue = new BigDecimal(insertedValue);
            invoiceDetailDto.setOfferUnitValue(offerValue);
        } else {
            invoiceDetailDto.setOfferUnitValue(new BigDecimal("0.00"));
        }

        invoiceDetailDto.setOfferDescription(localBinding.formItemDescription.getText().toString());

    }
    private void bindSpinners (ItemViewHolder holder, InvoiceDetailDto invoiceDetailDto) {

        holder.getBinding().itemQuantity.setOnItemSelectedListener
                (new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                        String selectedValue = adapterView.getItemAtPosition(position).toString();
                        Log.d(TAG, "Offer Quantity Selected Value: " + selectedValue);
                        invoiceDetailDto.setOfferQuantity(Integer.parseInt(selectedValue));
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        invoiceDetailDto.setOfferQuantity(1);
                    }
                });

        // ITEM TAX 1
        holder.getBinding().itemTax1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                                               long id) {
                        String selectedValue = adapterView.getItemAtPosition(position).toString();
                        try {
                            Number taxValue = new DecimalFormat("0.0#%").parse(selectedValue);
                            Log.d("InvoiceItemAdapter", "SelectedTax1: " + taxValue);
                            invoiceDetailDto.setItemUnitTax1(new BigDecimal(taxValue.toString()));
                            notifyDataSetChanged();
                        } catch (ParseException e) {
                            Log.e(TAG, "Error parsing value: " + e.getMessage());
                            throw new RuntimeException(e);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        invoiceDetailDto.setItemUnitTax1(BigDecimal.ZERO);
                    }
                }
        );

        // ITEM TAX 2
        holder.getBinding().itemTax2.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                        String selectedValue = adapterView.getItemAtPosition(position).toString();
                        try {
                            Number taxValue = new DecimalFormat("0.0#%").parse(selectedValue);
                            invoiceDetailDto.setItemUnitTax2(new BigDecimal(taxValue.toString()));
                            Log.d(TAG, "Selected Tax2: " + taxValue.toString());
                            notifyDataSetChanged();
                        } catch (ParseException e) {
                            Log.e(TAG, "Error parsing value: " + e.getMessage());
                            throw new RuntimeException(e);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        invoiceDetailDto.setItemUnitTax2(BigDecimal.ZERO);
                    }
                }
        );

        // ITEM TAX 3
        holder.getBinding().itemTax3.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                        String selectedValue = adapterView.getItemAtPosition(position).toString();
                        try {
                            Number taxValue = new DecimalFormat("0.0#%").parse(selectedValue);
                            invoiceDetailDto.setItemUnitTax3(new BigDecimal(taxValue.toString()));
                            Log.d(TAG, "Selected Tax3: " + taxValue.toString());
                            notifyDataSetChanged();
                        } catch (ParseException e) {
                            Log.e(TAG, "Error parsing value: " + e.getMessage());
                            throw new RuntimeException(e);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        invoiceDetailDto.setItemUnitTax3(BigDecimal.ZERO);
                    }
                }
        );
    }

    public void addItem(InvoiceDetailDto invoiceDetailDto) {
        invoiceItemList.add(invoiceDetailDto);
        int lastAddedPosition = invoiceItemList.size() - 1;
        lastAddedPositions.push(lastAddedPosition);
        notifyItemInserted(lastAddedPosition);
    }
    public void removeItem() {
        if(!lastAddedPositions.isEmpty() && (invoiceItemList.size() - 1) > 0) {
            int lastAddedPosition = lastAddedPositions.pop();
            invoiceItemList.remove(lastAddedPosition);
            notifyItemRemoved(lastAddedPosition);
        }
    }

}