package com.example.baas_invoice_android.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baas_invoice_android.R;
import com.example.baas_invoice_android.data.local.dto.InvoiceDetailDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceHeaderDto;
import com.example.baas_invoice_android.databinding.InvoicePreviewDialogBinding;
import com.example.baas_invoice_android.ui.adapters.InvoiceItemSummaryAdapter;

import java.util.List;

public class InvoicePreviewDialog extends DialogFragment {

    public static String TAG = "InvoicePreviewDialog";
    private final InvoiceHeaderDto header;
    private final List<InvoiceDetailDto> details;
    private InvoicePreviewDialogBinding invoicePreviewBinding;
    private RecyclerView invoiceItemSummaryRecycler;
    private InvoiceItemSummaryAdapter invoiceItemSummaryAdapter;
    public InvoicePreviewDialog(InvoiceHeaderDto headerDto, List<InvoiceDetailDto> invoiceDetailDtoList) {
        this.header = headerDto;
        this.details = invoiceDetailDtoList;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        invoicePreviewBinding = InvoicePreviewDialogBinding.inflate(inflater, container, false);
        View view = invoicePreviewBinding.getRoot();
        invoicePreviewBinding.setHeader(header);
        invoicePreviewBinding.setLifecycleOwner(getViewLifecycleOwner());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        invoiceItemSummaryRecycler = view.findViewById(R.id.invoice_item_summary_recycler);
        invoiceItemSummaryAdapter = new InvoiceItemSummaryAdapter(this.details);
        invoiceItemSummaryRecycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        invoiceItemSummaryRecycler.setAdapter(invoiceItemSummaryAdapter);
    }
}
