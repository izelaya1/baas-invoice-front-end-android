package com.example.baas_invoice_android.ui.adapters.binding;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

import com.google.android.material.textfield.TextInputEditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class InvoiceNumberBindingAdapter {

    @BindingAdapter("android:text")
    public static void setBigDecimalText(TextInputEditText view, BigDecimal value) {
        if(value != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            String formattedValue = decimalFormat.format(value);
            view.setText(formattedValue);

        }
        else {
            view.setText("");
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static BigDecimal getBigDecimalText(TextInputEditText view) {
        String text = view.getText().toString().replaceAll("[^\\\\d.]+", "");
        try {
            return new BigDecimal(text);
        }
        catch(NumberFormatException ex){
            return BigDecimal.ZERO;
        }
    }

//    @BindingAdapter("android:text")
//    public static void setIntegerText(TextInputEditText view, Integer value) {
//        view.setText(value != null ? String.valueOf(value) : "");
//    }
//
//    @InverseBindingAdapter(attribute = "android:text")
//    public static  Integer getIntegerText(TextInputEditText view) {
//        try {
//            return Integer.parseInt(view.getText().toString());
//        }
//        catch(NumberFormatException e) {
//            return null;
//        }
//    }


}
