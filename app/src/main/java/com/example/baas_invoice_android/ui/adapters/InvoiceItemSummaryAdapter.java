package com.example.baas_invoice_android.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baas_invoice_android.data.local.dto.InvoiceDetailDto;
import com.example.baas_invoice_android.databinding.InvoiceItemSummaryBinding;

import java.util.List;

public class InvoiceItemSummaryAdapter extends
        RecyclerView.Adapter<InvoiceItemSummaryAdapter.ItemViewHolder> {

    private static final String TAG = "InvoiceItemSummaryAdapter";
    private List<InvoiceDetailDto> invoiceDetailList;
    public InvoiceItemSummaryAdapter(List<InvoiceDetailDto> invoiceDetailList) {
        this.invoiceDetailList = invoiceDetailList;
    }
    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private InvoiceItemSummaryBinding binding;

        public ItemViewHolder(InvoiceItemSummaryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(InvoiceDetailDto invoiceDetailDto) {
            binding.setInvoiceDetailDto(invoiceDetailDto);
            binding.executePendingBindings();
        }

        public InvoiceItemSummaryBinding getBinding() {
            return this.binding;
        }
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        InvoiceItemSummaryBinding binding = InvoiceItemSummaryBinding
                .inflate(inflater, parent, false);

        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        InvoiceDetailDto invoiceDetailDto = invoiceDetailList.get(position);
        holder.bind(invoiceDetailDto);
    }

    @Override
    public int getItemCount() {
        return invoiceDetailList.size();
    }
}
