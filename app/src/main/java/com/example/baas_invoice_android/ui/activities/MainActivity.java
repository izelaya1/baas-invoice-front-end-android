package com.example.baas_invoice_android.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;


import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.baas_invoice_android.R;
import com.example.baas_invoice_android.ui.adapters.InvoiceFragmentAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    ActionBar actionBar;
    BottomNavigationView bottomNavigationView;
    private ViewPager2 viewPager;
    private InvoiceFragmentAdapter invoiceFormFragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_main);

        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        viewPager = findViewById(R.id.fragment_view_pager);
        invoiceFormFragmentAdapter = new InvoiceFragmentAdapter(this);

        actionBar = getSupportActionBar();
        actionBar.setTitle("Home");

        viewPager.setAdapter(invoiceFormFragmentAdapter);
        bottomNavigationView.setOnItemSelectedListener(this::onNavigationItemSelected);

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return displayFragment(item.getItemId());
    }

    private boolean displayFragment(int itemId){
        if(itemId == R.id.invoice_menu_item){
            viewPager.setCurrentItem(0);
            actionBar.setTitle("Invoice Generation");
            return true;
        }
        return false;
    }
    public InvoiceFragmentAdapter getInvoiceFormFragmentAdapter() {
        return this.invoiceFormFragmentAdapter;
    }
}