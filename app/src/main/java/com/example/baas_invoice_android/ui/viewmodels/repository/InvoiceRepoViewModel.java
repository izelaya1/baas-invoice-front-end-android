package com.example.baas_invoice_android.ui.viewmodels.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.baas_invoice_android.data.local.dto.InvoicePdfDto;
import com.example.baas_invoice_android.data.local.dto.InvoiceRequestDto;
import com.example.baas_invoice_android.data.models.entities.InvoiceEntity;
import com.example.baas_invoice_android.data.repository.InvoiceRepository;

import java.util.List;

import io.reactivex.Single;

public class InvoiceRepoViewModel extends ViewModel {
    private InvoiceRepository invoiceRepository;
    private LiveData<List<InvoiceEntity>> invoiceLiveData;
    public InvoiceRepoViewModel() {

    }
    public InvoiceRepoViewModel(InvoiceRepository invoiceRepository){
        this.invoiceRepository = invoiceRepository;
    }

    public Single<InvoiceEntity> generateInvoice(InvoiceRequestDto entity){
        return invoiceRepository.generateInvoice(entity);
                //.map(() ->)
                //.onErrorReturn();
    }

    //At this stage, it should be converted to pdf.
    public Single<InvoicePdfDto> getInvoiceBlob(String blobName) {
        return invoiceRepository.getInvoiceBlob(blobName);
    }

    public LiveData<List<InvoiceEntity>> getInvoiceLiveData(){
        return invoiceRepository.getInvoices();
    }

    public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }
}
